package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.isEmpty() || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        int currentCapacity = 0;
        for (int i = 1; i <= 50; i++)
            if (inputNumbers.size() == (i * (i + 1)) / 2)
                currentCapacity = i;

        if (currentCapacity == 0)
            throw new CannotBuildPyramidException();

        int lastRowLength = getNumbersInLastRow(currentCapacity);
        int[] ints = inputNumbers.stream().sorted(Comparator.reverseOrder()).mapToInt(i -> i).toArray();
        int[][] triangleField = new int[currentCapacity][lastRowLength];
        for (int[] oneRow : triangleField)
            Arrays.fill(oneRow, 0);

        int intsCounter = 0;
        for (int i = currentCapacity - 1; i >= 0; i--) {
            int tempRowLength = --lastRowLength;
            for (int j = 0; j <= i; j++) {
                triangleField[i][tempRowLength] = ints[intsCounter];
                intsCounter++;
                tempRowLength -= 2;
            }
        }
        return triangleField;
    }

    private int getNumbersInLastRow(int currentCapacity) {
        int lastRowLength = (currentCapacity * 2) - 1;
        return lastRowLength;
    }


}

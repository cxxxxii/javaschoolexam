package com.tsystems.javaschool.tasks.zones;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class RouteChecker {

    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */

    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds) {
        List<Integer> copyOfRequested = new ArrayList<>(requestedZoneIds);
        List<Integer> connectedZoneIds = new ArrayList<>();
        Iterator<Integer> requestedIterator = copyOfRequested.iterator();
        connectedZoneIds.add(requestedIterator.next());
        if (!hasZone(zoneState, connectedZoneIds.get(0)))
            return false;

        requestedIterator.remove();
        Boolean found;
        Integer id;
        for (Integer i = 0; i < connectedZoneIds.size(); i++) {
            requestedIterator = copyOfRequested.iterator();
            while (requestedIterator.hasNext()) {
                id = requestedIterator.next();
                found = false;
                List<Integer> neighbours = getZoneNeighboursById(zoneState, connectedZoneIds.get(i));
                for (Integer neighbourID : neighbours) {
                    if (Objects.equals(neighbourID, id)) {
                        addToConnected(connectedZoneIds, requestedIterator, id);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    neighbours = getZoneNeighboursById(zoneState, id);
                    for (Integer neighbourID : neighbours) {
                        if (Objects.equals(neighbourID, connectedZoneIds.get(i))) {
                            addToConnected(connectedZoneIds, requestedIterator, id);
                            break;
                        }
                    }
                }
            }
        }
        return Objects.equals(requestedZoneIds.size(), connectedZoneIds.size());
    }

    private boolean hasZone(List<Zone> zoneState, Integer id) {
        for (Zone zone : zoneState) {
            if (zone.getId() == id)
                return true;
        }
        return false;
    }

    private List<Integer> getZoneNeighboursById(List<Zone> zoneState, Integer requestedZoneId) {
        List<Integer> neighbours = null;
        for (Zone zone : zoneState) {
            if (zone.getId() == requestedZoneId) {
                neighbours = zone.getNeighbours();
            }
        }
        return neighbours;
    }

    private void addToConnected(List<Integer> connectedZoneIds, Iterator<Integer> requestedIterator, Integer id) {
        connectedZoneIds.add(id);
        requestedIterator.remove();
    }
}
